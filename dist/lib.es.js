import React from "react";
var Button$1 = "";
const Button = ({ text, type, onClick }) => {
  return /* @__PURE__ */ React.createElement("button", {
    className: `Button Button-${type}`,
    onClick
  }, text);
};
export { Button };
